import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { URLSearchParams } from '@angular/http';
import { UsersService, IUser, IQueryParams } from '../../services/users';
import { ActivatedRoute } from '@angular/router';
import { FormControl } from '@angular/forms';
import { serialize } from '../../utils/serialize';
import 'rxjs/add/operator/debounceTime';

@Component({
	selector: 'app-bot-users',
	templateUrl: './bot-users.component.html',
	styleUrls: ['./bot-users.component.scss']
})
export class BotUsersComponent implements OnInit {
	users: Array<IUser> = [];
	nextPageUrl: boolean | string = false;
	previousPageUrl: boolean | string = false;
	searchTerm: FormControl = new FormControl();
	queryParams: IQueryParams = {};

	constructor(
		private userService: UsersService,
		private route: ActivatedRoute,
		private location: Location
	) { }

	ngOnInit() {
		const { snapshot: { queryParams } } = this.route;
		const searchTerm = queryParams['searchTerm'] || '';
		this.searchTerm.setValue(searchTerm);

		this.route.queryParams.subscribe(res => console.log(res));

		if (searchTerm !== '') {
			this.queryParams.searchTerm = searchTerm;
		}

		this.searchTerm.valueChanges.debounceTime(400).subscribe(value => {
			this.queryParams.searchTerm = value;
			this.getList();
		});

		this.userService.users.subscribe(res => {
			const { result, nextPageUrl = false, previousPageUrl = false } = res;
			this.users = result;
			this.nextPageUrl = nextPageUrl;
			this.previousPageUrl = previousPageUrl;
		});
	}

	getList(): void {
		this.userService.getList(1, this.queryParams).subscribe(res => {
			const params = serialize(this.queryParams);
			this.location.replaceState('/users', params);
		});
	}
}
