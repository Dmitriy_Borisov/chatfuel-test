import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BotUsersComponent } from './bot-users.component';

describe('BotUsersComponent', () => {
	let component: BotUsersComponent;
	let fixture: ComponentFixture<BotUsersComponent>;

	beforeEach(
		async(() => {
			TestBed.configureTestingModule({
				declarations: [BotUsersComponent]
			}).compileComponents();
		})
	);

	beforeEach(() => {
		fixture = TestBed.createComponent(BotUsersComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
