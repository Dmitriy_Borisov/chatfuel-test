import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { UsersService, IUser } from '../../services/users';
import { statuses } from './constants';

@Component({
	selector: 'app-bot-user',
	templateUrl: './bot-user.component.html',
	styleUrls: ['./bot-user.component.scss']
})
export class BotUserComponent implements OnInit {
	user: IUser;
	form: FormGroup;
	success: Boolean = false;
	msg: String = '';

	constructor(
		private userService: UsersService,
		private route: ActivatedRoute,
		private location: Location,
		private fb: FormBuilder
	) { }

	ngOnInit() {
		const { snapshot: { data: { user } } } = this.route;
		this.user = user;
		this.initForm();
	}

	initForm(): void {
		this.form = this.fb.group({
			name: [this.user.name, Validators.required]
		});
	}

	onSubmit(user: IUser): void {
		if (this.form.invalid) {
			this.success = false;
			this.msg = statuses.REQUIRED_FIELD;
			return;
		}

		const { id } = this.user;
		this.userService.updateUser({ id, ...this.form.value }).subscribe(() => {
			this.success = true;
			this.msg = statuses.SUCCESS;
		}, err => {
			this.success = false;
			this.msg = err;
		});
	}

	goBack(): void {
		this.location.back();
	}
}
