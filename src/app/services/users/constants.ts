export interface IUser {
	id: string;
	name: string;
	avatarUrl: string;
}

export interface IUsers {
	result: Array<IUser>;
	nextPageUrl?: string;
	previousPageUrl?: string;
}

export interface IQueryParams {
	searchTerm?: string;
}
