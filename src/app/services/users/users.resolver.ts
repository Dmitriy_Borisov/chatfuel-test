import { Injectable } from '@angular/core';
import { UsersService } from './users.service';
import { IUser, IUsers } from './constants';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class UsersResolver implements Resolve<IUsers> {
	constructor(
		private usersService: UsersService,
		private router: Router
	) { }

	resolve(
		route: ActivatedRouteSnapshot,
		state: RouterStateSnapshot
	): Observable<any> | Promise<any> | any {
		const { params: { page }, queryParams } = route;

		return this.usersService.getList(page, queryParams).catch(res => {
			const { error } = res;
			if (error === 'NOT_FOUND') {
				this.router.navigate(['/404']);
			}

			return error;
		});
	}
}

@Injectable()
export class UserResolver implements Resolve<IUser> {
	constructor(
		private usersService: UsersService,
		private router: Router
	) { }

	resolve(
		route: ActivatedRouteSnapshot,
		state: RouterStateSnapshot
	): Observable<any> | Promise<any> | any {
		return this.usersService.getUserById(route.params.id).catch(res => {
			const { error } = res;
			if (error === 'NOT_FOUND') {
				this.router.navigate(['/404']);
			}

			return error;
		});
	}
}
