import { Injectable } from '@angular/core';
import { ApiService } from '../api/api.service';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { IUser, IUsers, IQueryParams } from './constants';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';

@Injectable()
export class UsersService {
	users = new BehaviorSubject<IUsers>({
		result: []
	});

	constructor(
		private api: ApiService
	) { }

	getList(page: string | number = 1, params: IQueryParams = {}): Observable<IUsers> {
		return this.api.get(`/users/${page}`, params).map(res => {
			this.users.next(res);
			return res;
		});
	}

	getUserById(id: string): Observable<IUser> {
		return this.api.get(`/user/${id}`).map(res => res.result);
	}

	updateUser(params: IUser): Observable<IUser> {
		const { id, ...user } = params;
		return this.api.post(`/user/${id}`, user).map(res => res.result);
	}
}
