export const API_ENDPOINT = '/api';

export interface IResponse {
	result?: any;
	error?: string;
	nextPageUrl?: string;
	previousPageUrl?: string;
}
