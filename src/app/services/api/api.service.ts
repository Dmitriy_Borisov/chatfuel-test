import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import 'rxjs/add/observable/of';
import { API_ENDPOINT, IResponse } from './constants';

@Injectable()
export class ApiService {
	private headers: HttpHeaders = new HttpHeaders({
		'Content-Type': 'application/json'
	});

	private responseHandler = (res: IResponse): any => {
		if (res.error) {
			throw Observable.throw(res.error);
		}

		return res;
	}

	private errorHandler = (err: IResponse): any => {
		console.warn(err);
		// TODO: error handler
	}

	constructor(
		private http: HttpClient
	) { }

	post(url: string, params?: object): Observable<any> {
		return this.http.post(`${API_ENDPOINT}${url}`, params, {
			headers: this.headers
		}).map(this.responseHandler, this.errorHandler);
	}

	get(url: string, params?: object): Observable<any> {
		return this.http.get(`${API_ENDPOINT}${url}`, {
			headers: this.headers,
			params: <HttpParams>params
		}).map(this.responseHandler, this.errorHandler);
	}

	put(url: string, params?: object): Observable<any> {
		return this.http.post(`${API_ENDPOINT}${url}`, params, {
			headers: this.headers
		}).map(this.responseHandler, this.errorHandler);
	}

	delete(url: string, params?: object): Observable<any> {
		return this.http.get(`${API_ENDPOINT}${url}`, {
			headers: this.headers,
			params: <HttpParams>params
		}).map(this.responseHandler, this.errorHandler);
	}
}
