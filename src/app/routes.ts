import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BotUsersComponent } from './containers/bot-users/bot-users.component';
import { BotUserComponent } from './containers/bot-user/bot-user.component';
import { UsersResolver, UserResolver } from './services/users/users.resolver';
import { NotFoundComponent } from './containers/not-found/not-found.component';

const routes: Routes = [
	{ path: '404', component: NotFoundComponent },
	{ path: 'users', component: BotUsersComponent, resolve: { users: UsersResolver } },
	{ path: 'users/:page', component: BotUsersComponent, resolve: { users: UsersResolver } },
	{ path: 'user/:id', component: BotUserComponent, resolve: { user: UserResolver } },
	{ path: '', redirectTo: '/users', pathMatch: 'full' },
	{ path: '**', redirectTo: '404'}
];

export const appRoutes: ModuleWithProviders = RouterModule.forRoot(routes);
