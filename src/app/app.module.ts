import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { appRoutes } from './routes';
import { AppComponent } from './app.component';
import { BotUsersComponent } from './containers/bot-users/bot-users.component';
import { BotUserComponent } from './containers/bot-user/bot-user.component';
import { NotFoundComponent } from './containers/not-found/not-found.component';
import { ApiService, UsersService, UsersResolver, UserResolver } from './services';

@NgModule({
	declarations: [
		AppComponent,
		BotUsersComponent,
		BotUserComponent,
		NotFoundComponent
	],
	imports: [
		BrowserModule,
		HttpClientModule,
		FormsModule,
		ReactiveFormsModule,
		appRoutes,
	],
	providers: [
		ApiService,
		UsersService,
		UsersResolver,
		UserResolver
	],
	bootstrap: [AppComponent]
})
export class AppModule {}
