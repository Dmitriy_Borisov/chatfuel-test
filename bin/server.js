const express = require('express');
const bodyParser = require('body-parser');
const port = 3000;
const limit = 10;
const users = require('./users.json');
const app = express();
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
const apiRouter = express.Router();

const getUserList = (page = 1, filter = '') => {
	const filtered = (!!filter) ? users.filter(u => u.name.toLowerCase().indexOf(filter.toLowerCase()) > -1) : users;
	const total = filtered.length;

	const start = (page - 1) * limit;
	const end = (start + limit > total) ? total : start + limit;

	if (start > total) {
		return {
			list: [],
			total: 0
		}
	}

	return {
		list: filtered.slice(start, end),
		total: total
	}
}

const responseUsers = (p = 1, query = {}) => {
	const page = parseInt(p, 10);
	const result = getUserList(page, query.searchTerm);
	const nextPageUrl = (page * limit < result.total) ? page + 1 : false;
	const previousPageUrl = (page <= 1) ? false : page - 1;

	if (result.total === 0 && page !== 1) {
		return { error: 'NOT_FOUND' };
	}

	const response = { result: result.list };
	if (nextPageUrl) {
		response.nextPageUrl = `/users/${nextPageUrl}`;
	}

	if (previousPageUrl) {
		response.previousPageUrl = `/users/${previousPageUrl}`;
	}

	return response;
}

apiRouter.get('/users', (req, res) => {
	res.send(responseUsers(1, req.query));
});

apiRouter.get('/users/:page', (req, res) => {
	res.send(responseUsers(req.params.page, req.query));
});

apiRouter.get('/user/:id', (req, res) => {
	const { params: { id } } = req;
	const result = users.find(u => u.id === id);
	const response = (result) ? { result } : { error: 'NOT_FOUND' };

	res.send(response);
});

apiRouter.post('/user/:id', (req, res) => {
	const { params: { id }, body } = req;
	const userIndex = users.findIndex(u => u.id === id);

	if (userIndex === -1) {
		return res.send({ error: 'NOT_FOUND' });
	}

	users[userIndex] = { ...users[userIndex], ...body };
	res.send({ response: users[userIndex] });
});

app.use('/api', apiRouter);

app.listen(port, () => {
	console.log(`API Server is listening on ${port}`);
});
